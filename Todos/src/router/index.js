import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Todos from '@/components/Todos'

Vue.use(Router)
// le routeur doit être activé (décomenté) dans App.vue pour être opérationnel.
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Todos',
      component: Todos
    },
    {
      path: '/jonny',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
})
